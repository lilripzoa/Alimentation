from django.shortcuts import render
from django.template import loader
from django.contrib import staticfiles
from .models import Pro, Association

def index(request):
    context = {
        "welcome_message": "Vous êtes sur la plateforme de mise en relation."
    }
    return render(request, "platformalim/index.html", context)

def show_pros_list(request):
    latest_pros_list = Pro.objects.order_by('-name')[:10]
    template = loader.get_template('platformalim/pros.html')
    context = {
        "welcome_message": "Vous êtes sur la page de listing des professionnels.",
        "pros_list": latest_pros_list,
    }
    return render(request, "platformalim/pros.html", context)

def show_assos_list(request):
    context = {
        "welcome_message": "Vous êtes sur la page de listing des associations."
    }
    return render(request, "platformalim/assos.html", context)