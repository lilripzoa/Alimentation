from django.db import models


class Pro(models.Model):
    email = models.EmailField("email de contact")
    name = models.CharField(max_length=200)

class Association(models.Model):
    name = models.CharField(max_length=200)
    email = models.EmailField("email de contact")

class Produit(models.Model):
    name = models.CharField(max_length=100)
    active = models.BooleanField(default=True)

    class ProduitType(models.TextChoices):
        PRODUIT_ALIMENTAIRE = "A"
        OBJET = "O"

    type = models.CharField(max_length=1, 
        choices=ProduitType,
        default=ProduitType.PRODUIT_ALIMENTAIRE,
    )

class DonFromPro(models.Model):
    date = models.DateField("Date du don")
    donateur = models.ForeignKey("Pro", on_delete=models.RESTRICT)

class DonProduit(models.Model):
    produit = models.ForeignKey('Produit', on_delete=models.RESTRICT)
    qty = models.FloatField()
    date_peremption = models.DateField("Date de péremption", null=True)

    class Unite(models.TextChoices):
        KILOS = "K"
        CAISSE = "C"
        LITRE = "L"
        UNITE = "U"

    qty_unity = models.CharField(max_length=1, default=Unite.KILOS)
    don_pro = models.ForeignKey('DonFromPro', related_name='dons_produits', on_delete=models.CASCADE)
    

class DonToAsso(models.Model):
    date = models.DateField("Date du don")
    donateur = models.ForeignKey("Association", on_delete=models.RESTRICT)

    
