from django.contrib import admin

# Register your models here.
from .models import Pro, Association, Produit, DonProduit, DonFromPro
#from models import Pro, Association

class ProAdmin(admin.ModelAdmin):
    fieldsets = [
        ("Nom",  {'fields': ['name']}),
        ("Email", {'fields': ['email']}),
    ]
    list_display = ('name', 'email')

class AssoAdmin(admin.ModelAdmin):
    fieldsets = [
        ("Nom",  {'fields': ['name']}),
        ("Email", {'fields': ['email']}),
    ]
    list_display = ('name', 'email')    

# Inline admin for DonProduit
class DonProduitInline(admin.TabularInline):  # or use admin.StackedInline
    model = DonProduit
    extra = 1  # Adds 1 extra row to the inline formset

# Admin model for DonFromPro
class DonFromProAdmin(admin.ModelAdmin):
    inlines = [DonProduitInline]

admin.site.register(Pro, ProAdmin)

admin.site.register(Association, AssoAdmin)

admin.site.register(Produit)

admin.site.register(DonProduit)

admin.site.register(DonFromPro, DonFromProAdmin)