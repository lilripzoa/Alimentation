from django.urls import path

from . import views

urlpatterns = [
    path("", views.index, name="index"),
    path("pros", views.show_pros_list, name="pros"),
    path("assos", views.show_assos_list, name="assos"),
]