from django.apps import AppConfig


class PlatformalimConfig(AppConfig):
    default_auto_field = 'django.db.models.BigAutoField'
    name = 'platformalim'
